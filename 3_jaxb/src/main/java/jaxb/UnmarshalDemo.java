package jaxb;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;
import java.io.File;

public class UnmarshalDemo {

    public static void main(String[] args) {
        try {
            JAXBContext context = JAXBContext.newInstance(Product.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();

            File file = new File("product.xml");

            Product product = (Product) unmarshaller.unmarshal(file);
            System.out.println("Na unmarshal:");
            System.out.println(product);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}